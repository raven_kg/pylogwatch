import bz2
import os
import re
import gzip
import lzma
import sqlite3
import importlib
import sentry_sdk
from datetime import datetime, timedelta

DEBUG=False

def item_import(name):
    d = name.rfind(".")
    classname = name[d+1:]
    m = importlib.__import__(name[:d], globals(), locals(), [classname], 1)
    return getattr(m, classname)

def before_send(event, hint):
    if event['extra'].get('event_time'):
        event['timestamp'] = event['extra']['event_time']
        event['extra'].pop('event_time', None)
    if DEBUG:
        print(f'Event: \n{event} \nHint: \n{hint}')
    # clean some unused shit before sending
    if event['extra'].get('log_file'):
        event['contexts'].pop('trace')
    # Causes error on old Sentry
    event.pop('transaction_info', None)
    event.pop('release', None)
    return event

class PyLog(object):

    def __init__(self, filenames, db_name='pylogwatch.db'):
        self.conn = self.init_db(db_name)
        self.curs = self.conn.cursor()
        self.fnames = filenames

    @staticmethod
    def init_db(db_name):
        """Set up the DB"""
        conn = sqlite3.connect(db_name)
        cur = conn.cursor()
        sql = ''' CREATE TABLE IF NOT EXISTS file_cursor (
                  filename TEXT PRIMARY KEY,
                  inode INTEGER, 
                  lastbyte INTEGER, 
                  updated INTEGER);'''
        cur.execute(sql)
        sql = """ CREATE TABLE IF NOT EXISTS events (
                  event TEXT PRIMARY KEY,
                  args TEXT,
                  updated INTEGER);"""
        cur.execute(sql)
        conn.commit()
        return conn

    def read_lines(self, f, lastpos=0):
        """Read full lines from the file object f starting from lastpos"""
        if hasattr(f, '_fp'):
            # bz2 or LZMA archive
            f_name = f._fp.name
        else:
            f_name = f.name
        self.save_fileinfo(f_name, os.stat(f_name).st_size, lastpos)
        f.seek(lastpos)
        while True:
            line = f.readline()
            if not line:
                return
            if isinstance(line, bytes):
                line = str(line, 'utf-8')
            # handle lines that are not yet finished (no \n)
            curpos = f.tell()
            if not line.endswith('\n'):
                f.seek(curpos)
                return
            yield line

    def get_fileinfo(self, fname):
        self.curs.execute(
            'SELECT filename, inode, lastbyte FROM file_cursor WHERE filename=?',
            [fname]
        )
        result = self.curs.fetchone()
        if result and len(result) == 3:
            f, inode, lastbyte = result
            return inode, lastbyte
        else:
            return None, 0

    def save_fileinfo(self, fname, inode, lastbyte):
        self.curs.execute(
            "REPLACE INTO file_cursor (filename, inode, lastbyte, updated) VALUES (?,?,?,datetime())",
            [fname, inode, lastbyte]
        )
        self.conn.commit()
        return

    def update_bytes(self, fname, lastbyte):
        """
        Only updates the lastbyte property of a file, without touching the inode.
        Meant for calling after each line is processed
        """
        def save_fileinfo(self, fname, inode, lastbyte):
            self.curs.execute(
                "UPDATE into file_cursor set lastbyte=? where filename=?",
                [fname, inode, lastbyte]
            )
            self.conn.commit()
            return

    def process_lines(self, fname, fileobject, lines):
        """Dummy line processor - should be overridden"""
        raise NotImplementedError

    def open_rotated_version(self, fname):
        dt_today = datetime.today().strftime('%Y%m%d')
        dt_yesterday = datetime.strftime(datetime.now() - timedelta(1), '%Y%m%d')
        suffixes = [
            '.0'
            '.1',
            '.1.gz',
            '.1.xz',
            '.1.lzma',
            '.1.bz2'
            '-{0}'.format(dt_today),
            '-{0}'.format(dt_yesterday),
            '-{0}.gz'.format(dt_today),
            '-{0}.gz'.format(dt_yesterday),
            '-{0}.xz'.format(dt_today),
            '-{0}.xz'.format(dt_yesterday),
            '-{0}.lzma'.format(dt_today),
            '-{0}.lzma'.format(dt_yesterday),
            '-{0}.bz2'.format(dt_today),
            '-{0}.bz2'.format(dt_yesterday)
        ]
        for suffix in suffixes:
            newname = fname + suffix
            if not os.path.exists(newname):
                continue
            try:
                if suffix.endswith('.xz') or suffix.endswith('.lzma'):
                    f = lzma.open(newname, 'r')
                elif suffix.endswith('.gz'):
                    f = gzip.open(newname, 'r')
                elif suffix.endswith('.bz2'):
                    f = bz2.open(newname, 'r')
                else:
                    f = open(newname, 'r')
                return f
            except Exception:
                continue

    def run(self):
        for fn in self.fnames:
            if not os.path.exists(fn):
                continue

            newlines = []
            rotated = None
            lastinode, lastbyte = self.get_fileinfo(fn)
            if lastbyte and not lastinode == os.stat(fn)[1]:
                # handle rotated files
                rotated = self.open_rotated_version(fn)
                if rotated:
                    newlines = self.read_lines(rotated, lastbyte)
                    lastbyte = 0
                    self.process_lines(fn, rotated, newlines)
            try:
                f = open(fn)
            except Exception:
                continue
            self.process_lines(fn, f, self.read_lines(f, lastbyte))
            lastbyte = f.tell()
            lastinode = os.stat(fn)[1]
            f.close()
            self.save_fileinfo(fn, lastinode, lastbyte)
            if rotated:
                rotated.close()


class PyLogConf(PyLog):
    def __init__(self, conf, debug=False):
        """
        Initialize object based on the provided configuration
        """
        self.conf = conf
        self.formatters = {}
        self.debug = self.conf.DEBUG
        # Override config value by command line argument
        if debug:
            self.debug = debug
        try:
            environment = str(self.conf.ENV)
        except Exception:
            environment = 'production'
        sentry_sdk.init(
            dsn=conf.RAVEN['dsn'],
            debug=self.debug,
            environment=environment,
            release='pylogwatch-0.2.0',
            before_send=before_send,
            send_client_reports=False,
            enable_tracing=False
        )
        for k, v in self.conf.FILE_FORMATTERS.items():
            if isinstance(v, str):
                raise ValueError('Please use a list or a tuple for the file formatters values')
            self.formatters[k] = [item_import(i)() for i in v]
        try:
            dbname = self.conf.DB_NAME
        except AttributeError:
            dbname = 'pylogwatch.db'
        db_name = os.path.join(os.path.dirname(conf.__file__), dbname)
        return super().__init__(self.conf.FILE_FORMATTERS.keys(), db_name=db_name)

    def process_lines(self, fname, fileobject, lines):
        """Main workhorse. Called with the filename that is being logged and an iterable of lines"""
        global DEBUG
        if self.debug:
            DEBUG = True
        for line in lines:
            with sentry_sdk.push_scope() as scope:
                scope.clear()
                paramdict = {}
                data = {'message': line.replace('%', '%%')}
                for fobj in self.formatters[fname]:
                    fobj.format_line(line, data, paramdict)
                skip = False

                if self.conf.STOPWORDS:
                    for key in self.conf.STOPWORDS:
                        if key in data['message']:
                            skip = True
                    if skip:
                        continue
                if hasattr(self.conf, 'FILTER_REGEX'):
                    f_regex = re.compile(r'{0}'.format(self.conf.FILTER_REGEX))
                    if re.match(f_regex, data['message']):
                        continue
                if not data.pop('_do_not_send', False):  # Skip lines that have the '_do_not_send' key
                    message = data['message']
                    if paramdict:
                        message = data['message'] % tuple([paramdict[i] for i in sorted(paramdict.keys())])
                    if self.debug:
                        print(data)
                    scope.set_extra('log_file', fname)
                    if data.get('date'):
                        scope.set_extra('event_time', data['date'])
                    sentry_sdk.capture_message(message)
                    self.update_bytes(fname, fileobject.tell())
