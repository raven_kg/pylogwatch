import re
from dateutil.parser import parse
from sentry_sdk import set_tag, set_level
from pylogwatch.formatters.base import BaseFormatter


class MySQLErrorLogFormatter(BaseFormatter):
    """
    Relies on the following parts:
    <YYYY>-<mm>-<dd>T<HH>:<MM>:<SS>.<nnnnnn>Z <thread_id> [<level>] [MY-<num>] [<error-source>] <msg>
    """

    def format_line(self, line, datadict, paramdict):

        try:
            dt = parse(line[:27])
        except ValueError:
            return datadict
        set_tag('service', 'MySQL')
        # Add date as a param and event date
        datadict['message'] = self.replace_param(line, datadict['message'], '%s' % line[0:27], paramdict)
        datadict['date'] = dt

        parts = [p.strip().lstrip('[') for p in line[28:].split(']')]
        thread_id = parts[0].split('[')[0].strip()
        log_level = parts[0].split('[')[1].upper()
        log_level = self.fix_loglevel(log_level)

        if not log_level.isdigit() and log_level in self.LOG_LEVELS:
            set_level(log_level.lower())
        else:
            set_level('info')

        # Tag mysqld error code
        error_code = re.findall(r'(MY-\d+)', parts[1])
        if error_code:
            set_tag('error_code', error_code[0])

        if re.match(r'\w+', parts[2]):
            set_tag('subsystem', parts[2])

        if thread_id:
            datadict['message'] = self.replace_param(line, datadict['message'], thread_id.strip(), paramdict)
