import re
import socket
from dateutil.parser import parse
from sentry_sdk import set_tags, set_level
from pylogwatch.formatters.base import BaseFormatter
from pprint import pprint

# IP address regex
# taken from this gist https://gist.github.com/dfee/6ed3a4b05cfe7a6faf40a2102408d5d8
IPV4SEG  = r'(?:25[0-5]|(?:2[0-4]|1{0,1}[0-9]){0,1}[0-9])'
IPV4ADDR = r'(?:(?:' + IPV4SEG + r'\.){3,3}' + IPV4SEG + r')'
IPV6SEG  = r'(?:(?:[0-9a-fA-F]){1,4})'
IPV6GROUPS = (
    r'(?:' + IPV6SEG + r':){7,7}' + IPV6SEG,                  # 1:2:3:4:5:6:7:8
    r'(?:' + IPV6SEG + r':){1,7}:',                           # 1::                                 1:2:3:4:5:6:7::
    r'(?:' + IPV6SEG + r':){1,6}:' + IPV6SEG,                 # 1::8               1:2:3:4:5:6::8   1:2:3:4:5:6::8
    r'(?:' + IPV6SEG + r':){1,5}(?::' + IPV6SEG + r'){1,2}',  # 1::7:8             1:2:3:4:5::7:8   1:2:3:4:5::8
    r'(?:' + IPV6SEG + r':){1,4}(?::' + IPV6SEG + r'){1,3}',  # 1::6:7:8           1:2:3:4::6:7:8   1:2:3:4::8
    r'(?:' + IPV6SEG + r':){1,3}(?::' + IPV6SEG + r'){1,4}',  # 1::5:6:7:8         1:2:3::5:6:7:8   1:2:3::8
    r'(?:' + IPV6SEG + r':){1,2}(?::' + IPV6SEG + r'){1,5}',  # 1::4:5:6:7:8       1:2::4:5:6:7:8   1:2::8
    IPV6SEG + r':(?:(?::' + IPV6SEG + r'){1,6})',             # 1::3:4:5:6:7:8     1::3:4:5:6:7:8   1::8
    r':(?:(?::' + IPV6SEG + r'){1,7}|:)',                     # ::2:3:4:5:6:7:8    ::2:3:4:5:6:7:8  ::8       ::
    r'fe80:(?::' + IPV6SEG + r'){0,4}%[0-9a-zA-Z]{1,}',       # fe80::7:8%eth0     fe80::7:8%1  (link-local IPv6 addresses with zone index)
    r'::(?:ffff(?::0{1,4}){0,1}:){0,1}[^\s:]' + IPV4ADDR,     # ::255.255.255.255  ::ffff:255.255.255.255  ::ffff:0:255.255.255.255 (IPv4-mapped IPv6 addresses and IPv4-translated addresses)
    r'(?:' + IPV6SEG + r':){1,4}:[^\s:]' + IPV4ADDR,          # 2001:db8:3:4::192.0.2.33  64:ff9b::192.0.2.33 (IPv4-Embedded IPv6 Address)
)
IPV6ADDR = '|'.join(['(?:{})'.format(g) for g in IPV6GROUPS[::-1]])  # Reverse rows for greedy match

IP_RE = re.compile(r'.* (' + IPV4ADDR + '|' + IPV6ADDR + ')')


def resolve_host(ip):
    try:
        return socket.gethostbyaddr(ip)[0]
    except (socket.herror, IndexError):
        pass


class NginxErrorLogFormatter(BaseFormatter):
    """
    Relies on the following parserts:
    <year/month/day hour:minute:sec> [<severity>] <cryptic_numbers> <error_description> client: <client>,
        server: <client>, request: <request>, host: <host>
    """
    activate_on_fname_suffix = ('error.log', 'error_log')

    def format_line(self, line, datadict, paramdict):
        try:
            dt = parse(line[:19])
        except ValueError:
            return datadict

        # Add date as a param and event date
        datadict['message'] = self.replace_param(line, datadict ['message'], '%s' % line[0:19], paramdict)
        datadict['date'] = dt
        datadict['geo'] = {}

        tags = {
            "service": "nginx",
            "remote_ip": None,
            "remote_host": None,
            "domain":  None,
            "referer":   None,
            "upstream":  None
        }

        # Add remote IP as a param
        client_ip = re.findall(r'client: (' + IPV4ADDR + '|' + IPV6ADDR + ')\,', line)
        if client_ip:
            tags.update({
                'remote_host': resolve_host(client_ip[0]),
                'remote_ip': client_ip[0]
            })
            datadict['message'] = self.replace_param(line, datadict['message'], client_ip[0], paramdict)

            # GeoIP stuff
            geo = self.get_geo(client_ip[0])
            # Enrich internal sentry variables
            datadict['geo']['country_code'] = geo.pop('Country_code', None)
            datadict['geo']['region'] = geo['Country']
            datadict['geo']['city'] = geo['City']
            # Add to tags for better grouping
            for k, v in geo.items():
                if v:
                    tags.update({f'GeoIP.{k}': v})

        # Add upstream to tags
        upstream = re.findall(r' upstream: "(.+?)"', line)
        if upstream:
            _reg = re.compile('((.*?)//(.*?)\:\d+)/(.+)')
            _upstream = re.sub(_reg, '\g<1>', upstream[0])
            tags['upstream'] = _upstream

        # Add server name
        server = re.findall(r'host: "(.+?)"', line)
        if server:
            datadict ['message'] = self.replace_param(line, datadict ['message'], server[0], paramdict)
            tags['domain'] = server[0]


        # Set log level
        severity = [p.strip().lstrip('[') for p in line[20:].split(']')][0]
        log_level = severity.upper()
        if log_level:
            log_level = self.fix_loglevel(log_level)

        if not log_level.isdigit() and log_level in self.LOG_LEVELS:
            set_level(log_level.lower())

        # set the Referer field as the culprit
        ref = line.split('referrer: ')[-1]
        if ref != line:
            datadict['culprit'] = ref.strip().strip('"')

        # get rid of some garbage
        datadict['message'] = re.sub(r'\]\s(\d+#\d+:\s)', '] ', datadict['message'])
        datadict['message'] = re.sub(r'\*\d{5,}\s', '', datadict['message'])

        set_tags(tags)


class ApacheErrorLogFormatter(BaseFormatter):
    """
    Relies on the following parts:
    [date] [severity] [client XXX] everything else
    """
    activate_on_fname_suffix = ('error.log', 'error_log')

    def format_line(self, line, datadict, paramdict):
        line_parts = [p.strip().lstrip('[') for p in line.split(']')]
        try:
            dt = parse(line_parts[0])
            dt_formatted = dt.strftime('%Y/%m/%d %H:%M:%S')
        except ValueError:
            return datadict
        # Add date as a param and event date
        datadict['message'] = self.replace_param(line, datadict['message'], f'[{dt_formatted}]', paramdict)
        datadict ['date']= dt
        datadict['geo'] = {}

        # Add remote IP as a param
        if len(line_parts) > 3 and IP_RE.match(line_parts[2]):
            datadict['message'] = self.replace_param(line, datadict['message'], line_parts[2].split()[-1], paramdict)
        tags = {
            'service': 'Apache',
            'domain': None,
            'remote_host': None,
            'remote_ip': None,
            'apache_module': None,
        }

        # Tag virtualhost for MPM-ITK 503 error
        vhost = re.findall(r'MaxClientsVhost reached for (.+), refusing client', line)
        if vhost:
            tags['domain'] = re.sub(r':(\d+)?', '', vhost[0])
            datadict['message'] = self.replace_param(line, datadict['message'], vhost[0], paramdict)

        # Tag client IP
        client_ip = re.findall(IP_RE, line)
        if client_ip:
            tags.update({
                'remote_host': resolve_host(client_ip[0]),
                'remote_ip': client_ip[0]
            })
            # GeoIP stuff
            geo = self.get_geo(client_ip[0])
            # Enrich internal sentry variables
            datadict['geo']['country_code'] = geo.pop('Country_code', None)
            datadict['geo']['region'] = geo['Country']
            datadict['geo']['city'] = geo['City']
            # Add to tags for better grouping
            for k, v in geo.items():
                if v:
                    tags.update({f'GeoIP.{k}': v})

        # Tag apache error code
        apache_error = re.findall(r'(AH\d{4,5})', line)
        if apache_error:
            tags['apache_code'] = apache_error[0]

        # Set log level
        try:
            log_level = line_parts[1].upper()
            # Apache 2.4 log levels contains module name.
            if re.match(r'.+:.+', log_level):
                tags['apache_module'] = log_level.split(':')[0].lower()
                log_level = self.fix_loglevel(log_level.split(':')[1])
        except IndexError:
            log_level = 'INFO'
        if not log_level.isdigit() and log_level in self.LOG_LEVELS:
            set_level(log_level.lower())

        # set the Referer field as the culprit
        ref = line.split('referer: ')[-1]
        if ref != line:
            datadict['culprit'] = ref.strip()

        # get rid of some garbage
        datadict['message'] = re.sub(line_parts[0], dt_formatted, datadict['message'])
        datadict['message'] = re.sub(r'(\[pid\s\d+:tid\s\d+\]\s)', '', datadict['message'])
        datadict['message'] = re.sub(r'(AH\d{4,}:\s)', '', datadict['message'])

        set_tags(tags)


class FPMErrorLogFormatter(BaseFormatter):
    """
    Relies on the following parts:
    [DD-MON-YYY HH:MM:SS] severity: pid NUM [pool name] error message
    """
    activate_on_fname_suffix = ('error.log', 'error_log')

    def format_line (self, line, datadict, paramdict):
        log_level = None
        try:
            log_level = re.findall(r'^([A-Z]+):', line[23:])[0]
        except IndexError:
            pass

        try:
            log_level = re.findall(r'^([A-Z]+):', line[30:])[0]
        except IndexError:
            pass

        try:
            if log_level and log_level.upper() == 'DEBUG':
                dt = parse(line[1:28])
            else:
                dt = parse(line[1:21])
        except ValueError:
            return datadict

        tags = {
            'service': 'PHP-FPM',
            'pool': 'master process',
            'pid': None
        }

        # Add date as a param and event date
        if log_level and log_level.upper() == 'DEBUG':
            datadict['message'] = self.replace_param(line, datadict['message'], '%s' % line[1:28], paramdict)
        else:
            datadict['message'] = self.replace_param(line, datadict ['message'], '%s' % line[1:21], paramdict)
        datadict['date'] = dt

        # Set log level
        if not log_level.isdigit():
            log_level = self.fix_loglevel(log_level)
            if log_level in self.LOG_LEVELS:
                set_level(log_level.lower())

        # Add pool name
        pool = re.findall(r'\[pool (\w+)\]', line)
        if pool:
            tags['pool'] = pool[0]

        # Process pid
        pid = re.findall(r'pid (\d+)(,|\n)', line)
        if pid:
            tags['pid'] = pid[0][0]

        set_tags(tags)
