#!/usr/bin/env python3
# Main executable

import os
import sys
import inspect
from argparse import ArgumentParser
from importlib.machinery import SourceFileLoader

current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(current_dir)
sys.path.insert(0, parent_dir)

from pylogwatch.logwlib import PyLogConf
from pylogwatch.utils import lockfile


def load_cfg_module(cfg_path):
    try:
        return SourceFileLoader('PyLogConfig', os.path.realpath(cfg_path)).load_module()
    except ImportError as err:
        sys.exit('Cannot load config file %s: %s' % (cfg_path, err))


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('-c', '--config', default="~/pylogconf.py",
                   help="Filesystem path to (python) configuration file [default: %default]")
    parser.add_argument('-d', '--debug', action='store_true', help='Display parsed strings into stdout')
    args = parser.parse_args()

    # Check if we can obtain a lock - make sure we're the only process on this config
    lock_fn = os.path.realpath(args.config) + '.lck'
    lock_fd = open(lock_fn, 'w')
    if not lockfile(lock_fd):
        sys.exit('Cannot obtain a lock on %s' %  lock_fn)

    cfg_mod = load_cfg_module(args.config)
    pl = PyLogConf(cfg_mod, args.debug)
    pl.run()
