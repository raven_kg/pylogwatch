from setuptools import setup

with open('requirements.txt') as reqs_file:
    requirements = reqs_file.read().splitlines()

with open('README.txt') as readme_file:
    readme = readme_file.read()

description = 'Python utility to parse log files and send them to a Sentry server.'

setup(
    name='PyLogWatch',
    version='0.3.0',
    author='E. Filipov, MTR Design',
    author_email='pylogwatch@mtr-design.com',
    maintainer='A. Korolkov',
    maintainer_email='raven_kg@mail.ru',
    packages=['pylogwatch', 'pylogwatch.formatters'],
    scripts=['bin/pylog.py'],
    url='https://bitbucket.org/raven_kg/pylogwatch',
    license='LICENSE.txt',
    description=description,
    long_description=readme,
    python_requires='>=3.5',
    install_requires=requirements,
)
